var _gaq = _gaq || [];
var usuarioUA = "UA-9075940-1";
if (window.location.host == "localhost" || window.location.host == "217.16.255.57") {
    usuarioUA = "UA-9075940-7";
}
_gaq.push(['_setAccount', usuarioUA]);
_gaq.push(["_setCustomVar", 1, 'Seccion', 'Portal', 3]);
_gaq.push(['_setCampaignCookieTimeout', 2592000000]);
var tipologiasSVH = {
    "vivienda": ["piso", "casa", "casa-adosada", "atico", "apartamento", "planta-baja", "duplex", "flat", "house", "penthouse", "apartment", "ground-floor", "pis", "casa", "casa-adossada", "atic", "apartament", "planta-baixa", "venta/vivienda", "venda/habitatge", "buying/home"],
    "local": ["local", "shop-premises"],
    "oficina": ["oficina", "office", "oficina"],
    "plaza-de-parking": ["plaza-de-parking", "parking-space", "placa-de-garatge"],
    "trastero": ["trastero", "storeroom", "traster"],
    "naves": ["naves", "industrial-premises", "naus"],
    "terreno": ["terreno", "land", "terreny"],
    "varios": ["varios", "various", "diversos"],
    "edificio": ["edificio", "building", "edifici"],
    "hotel": ["hotel", "hotel", "hotel"]
};

function urlActual() {
    var urlA = window.location.href.toString().split(window.location.host)[1];
    return urlA;
}

function checkIfURL(url) {
    var url_Actual = urlActual();
    if (url_Actual.indexOf(url) > 0) {
        return true;
    }
    return false;
}

function esFichaInmueble() {
    var miurl = urlActual();
    if (miurl.match("^\/(venta|venda|buying)\/.*\/[0-9]+$")) {
        return true;
    }
    return false;
}
url_Actual = urlActual();

function trackingAnalyticsBorrar(e) {
    try {
        var seccionPrivada = null;
        url_Actual = urlActual();
        if (url_Actual == "/svhPortal" || url_Actual == "/index.jsp") {
            var seccionActual = jQuery(".hiloAriadna").html();
            if (seccionActual.indexOf("Zona privada")) {
                if (seccionActual.indexOf("Resumen")) {
                    seccionPrivada = "Resumen";
                } else if (seccionActual.indexOf("Propuestas de precio")) {
                    seccionPrivada = "Propuestas de precio";
                } else if (seccionActual.indexOf("Mis favoritos")) {
                    seccionPrivada = "Mis favoritos";
                } else if (seccionActual.indexOf("Búsquedas")) {
                    seccionPrivada = "Búsquedas";
                } else if (seccionActual.indexOf('Revista "Pásalo"')) {
                    seccionPrivada = 'Revista "Pásalo"';
                } else {
                    seccionPrivada = "Otros";
                }
            } else if (seccionActual.indexOf('Mi cuenta')) {
                seccionPrivada = "Mi cuenta";
            }
        }
        var sender = (e && e.target) || (window.event && window.event.srcElement);
        if (typeof sender !== 'undefined') {
            if (sender.tagName !== 'undefined') {
                var mandarEventoGA = 'Log elementos: ';
                if (seccionPrivada) {
                    mandarEventoGA += '(' + seccionPrivada + ') ';
                }
                if ( !! sender.parentNode) {
                    mandarEventoGA += "Parent: tag='" + sender.parentNode.tagName + "' class='" + sender.parentNode.className + "'" + " id='" + sender.parentNode.id + "' > ";
                }
                if ( !! sender.previousSibling.previousSibling) {
                    mandarEventoGA += "Elemento Anterior: tag='" + sender.previousSibling.previousSibling.tagName + "' class='" + sender.previousSibling.previousSibling.className + "'" + " id='" + sender.previousSibling.previousSibling.id + "' | ";
                }
                mandarEventoGA += "Elemento: tag='" + sender.tagName + "' class='" + sender.className + "'" + " id='" + sender.id + "' name='" + sender.name + "'";
                if ( !! sender.nextSibling.nextSibling) {
                    mandarEventoGA += " | Elemento Posterior: tag='" + sender.nextSibling.nextSibling.tagName + "' class='" + sender.nextSibling.nextSibling.className + "'" + " id='" + sender.nextSibling.nextSibling.id + "'";
                }
                trackEventoParticular('Debug', mandarEventoGA, 'Desde: ' + url_Actual);
            }
        } else {
            var noMasCaller = 0;
            var funcActual = arguments.callee.caller;
            var path = [];
            var callerFunction;
            var mandarEventoGA = 'Log funciones: ';
            while (noMasCaller == 0) {
                callerFunction = (funcActual.toString().match(/function ([^\(]+)/) === null) ? false : funcActual.toString().match(/function ([^\(]+)/)[1];
                if (callerFunction !== false) {
                    path.push(callerFunction + '()');
                    funcActual = funcActual.caller;
                } else {
                    noMasCaller = 1;
                }
            }
            var cadena = 'trackingAnalyticsBorrar()';
            for (i = 0; i < path.length; i++) {
                if (i == 0) cadena = path[i] + ' > trackingAnalyticsBorrar()';
                else cadena = path[i] + ' > ' + cadena;
            }
            if (seccionPrivada) {
                mandarEventoGA += '(' + seccionPrivada + ') ';
            }
            trackEventoParticular('Debug', mandarEventoGA + cadena, 'Desde: ' + url_Actual);
        }
    } catch (err) {}
}
var $gaAvl = (function() {
    if (typeof _gaq !== 'undefined') {
        return true;
    } else {
        return false;
    }
})();
jQuery(document).ready(function() {
    if (esFichaInmueble()) {
        jQuery("#nombre2, #paistelefono2, #telefono2, #email2, #codigoAmigo2, #mensaje2, #condiciones_uso2").each(function() {
            jQuery(this).blur(function() {
                _gaq.push(['_trackEvent', 'Formulario Derecha', 'Formulario en: ' + window.location.protocol + "//" + window.location.host + window.location.pathname, 'campo iniciado: ' + jQuery(this).attr("id")]);
            });
        });
        jQuery("#nombre1, #paistelefono1, #telefono1, #email1, #codigoAmigo1, #mensaje1, #condiciones_uso1").each(function() {
            jQuery(this).blur(function() {
                _gaq.push(['_trackEvent', 'Formulario Inferior', 'Formulario en: ' + window.location.protocol + "//" + window.location.host + window.location.pathname, 'campo iniciado: ' + jQuery(this).attr("id")]);
            });
        });
    }
    jQuery("a").on("click", function() {
        var escapar_href = jQuery(this).attr("href").replace("'", "\'").replace('"', '\"');
        var bhost = jQuery(this).prop("hostname");
        var wbhost = bhost.split('.').reverse();
        if (typeof(wbhost[1]) != "undefined" && wbhost[1] != '' && typeof(wwwhost[0]) != "undefined" && wwwhost[0] != '') {
            var wwbhost = wbhost[1] + '.' + wbhost[0];
            var whost = window.location.hostname;
            var wwhost = whost.split('.').reverse();
            var wwwhost = wwhost[1] + '.' + wwhost[0];
            if (wwbhost !== wwwhost) {
                if ($gaAvl) {
                    _gaq.push(['_trackEvent', 'Enlace saliente', 'Enlace saliente: ' + escapar_href, 'desde: ' + window.location.pathname]);
                }
            }
        }
    });
});
var svh_gaq_listo = false;
var svh_gaq_intentos = 0;
svh_apis_listo();

function svh_extractParamFromUri(uri, paramName) {
    if (!uri) {
        return;
    }
    var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)');
    var params = regex.exec(uri);
    if (params != null) {
        return unescape(params[1]);
    }
    return;
}

function svh_trackTwitter(intent_event) {
    if (intent_event) {
        var opt_pagePath;
        if (intent_event.target && intent_event.target.nodeName == 'IFRAME') {
            opt_target = svh_extractParamFromUri(intent_event.target.src, 'url');
        }
        if (svh_gaq_listo) _gaq.push(['_trackSocial', 'twitter', 'tweet', opt_pagePath]);
    }
}

function svh_apis_listo() {
    if (typeof FB !== 'undefined' && typeof FB.Event !== 'undefined' && typeof FB.Event.subscribe !== 'undefined' && typeof _gaq !== 'undefined' && typeof twttr !== 'undefined') {
        FB.Event.subscribe('edge.create', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
        });
        FB.Event.subscribe('edge.remove', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
        });
        FB.Event.subscribe('message.send', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]);
        });
        twttr.ready(function(twttr) {
            twttr.events.bind('tweet', svh_trackTwitter);
        });
        svh_gaq_listo = true;
    } else {
        if (svh_gaq_intentos < 3) {
            setTimeout("svh_apis_listo()", 200);
            svh_gaq_intentos++;
        }
    }
}

var typo = false;
for (i in tipologiasSVH) {
    for (var j = 0; j <= tipologiasSVH[i].length - 1; j++) {
        if (!typo) {
            if (checkIfURL(tipologiasSVH[i][j])) {
                typo = i;
            }
        }
    }
}
if (typo && esFichaInmueble()) {
    _gaq.push(["_setCustomVar", 4, 'Tipologia', typo, 3]);
}

jQuery(document).ready(function() {
    var typo = false;
    for (i in tipologiasSVH) {
        for (var j = 0; j <= tipologiasSVH[i].length - 1; j++) {
            if (!typo) {
                if (i == "vivienda") {
                    if (checkIfURL(tipologiasSVH[i][j])) {
                        s = jQuery(".estadoDeUsoTD > h2 > span").text().trim();
                        if (s == "Promoción a estrenar" || s == "Promoció a estrenar" || s == "New construction promotion") {
                            typo = 'promocion a estrenar';
                        } else if (s == "Promoción de segunda mano" || s == "Promoció de segona mà" || s == "Second hand promotion") {
                            typo = 'promocion segunda mano';
                        }
                    }
                }
            }
        }
    }
    if (typo && esFichaInmueble()) {
        _gaq.push(["_setCustomVar", 2, 'Promociones', typo, 3]);
    }
    if (url_Actual == "/svhPortal") {
        try {
            jQuery("#home-search-go").on("click", function() {
                url_Actual = urlActual();
                trackEventoParticular('Microconversiones', 'buscar', 'Desde: ' + url_Actual);
            });
        } catch (err) {}
    }
    try {
        jQuery("#btn_buscar").on("click", function() {
            var url_Actual = urlActual();
            trackEventoParticular('Microconversiones', 'buscar', 'Desde: ' + url_Actual);
        });
    } catch (err) {}
    try {
        jQuery(".idz_btn > #button_online > a").on("click", function() {
            var url_Actual = urlActual();
            trackEventoParticular('Microconversiones', 'iniciar-chat', 'Desde: ' + url_Actual);
        });
        jQuery("#idz_btn > #idz_fonline > a").on("click", function() {
            var url_Actual = urlActual();
            trackEventoParticular('Microconversiones', 'iniciar-chat', 'Desde: ' + url_Actual);
        });
    } catch (err) {}
    var SVHes = (typeof jQuery(".header ul > li").find("a[lang=es]")[0] == "object") ? true : false;
    var SVHca = (typeof jQuery(".header ul > li").find("a[lang=ca]")[0] == "object") ? true : false;
    var SVHen = (typeof jQuery(".header ul > li").find("a[lang=en]")[0] == "object") ? true : false;
    if (SVHes) {
        if (SVHca) {
            _gaq.push(['_setCustomVar', 3, 'idioma', 'en', 3]);
        } else {
            _gaq.push(['_setCustomVar', 3, 'idioma', 'cat', 3]);
        }
    } else {
        _gaq.push(['_setCustomVar', 3, 'idioma', 'es', 3]);
    }

    _gaq.push(['_trackEvent', 'No Interactivo', '-', '-', 0, true]);
});

function codigoLoginGA() {}

function codigoPrivadaGA() {
    trackingAnalyticsBorrar();
}

function trackPageviewGenerico(url, params) {
    var pageURL = url + params;
    try {
        trackingAnalyticsBorrar();
        _gaq.push(['_trackPageview', pageURL]);
    } catch (err) {}
}

function trackEventoParticular(categoria, accion, etiqueta) {
    try {
        _gaq.push(['_trackEvent', categoria, accion, etiqueta]);
    } catch (err) {}
}

function contacto_venta(parametro) {
    var url_Actual = urlActual();
    if (parametro == "frmMasInfo2") {
        _gaq.push(['_trackPageview', "/conversiones/contacto/derecha"]);
    } else if (parametro == "frmMasInfo1") {
        _gaq.push(['_trackPageview', "/conversiones/contacto/inferior"]);
    }
}

function estadisticasGoogleAnaliticsBuscador(parametrosBusqueda) {
    parametro = parametrosBusqueda.split("-");
    provincia = parametro[2];
    _gaq.push(["_setCustomVar", 4, 'Tipologia Parilla', parametro[0], 3]);
    _gaq.push(["_setCustomVar", 5, 'Provincia', provincia, 3]);
    _gaq.push(['_deleteCustomVar', 2]);
    trackPageviewGenerico('resultados/buscador/?pal=', parametrosBusqueda);
}

function estadisticasGoogleAnaliticsBuscadorMapa(palabraClave) {
    trackPageviewGenerico('resultados/mapa/', palabraClave);
}

function estadisticasGoogleAnaliticsBuscadorRefencia(numeroReferencia) {
    trackPageviewGenerico('resultados/referencia/', numeroReferencia);
}

function estadisticasGAAltaUsuario() {}

function estadisticasGAAltaUsuarioConfirm() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'registrame', 'Desde: ' + url_Actual);
}

function estadisticasGAAlertaMail() {}

function estadisticasGAAlertaMailConfirm() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'inmuebles-similares', 'Desde: ' + url_Actual);
}

function estadisticasGAGuardarFavoritos() {}

function estadisticasGAGuardarFavoritosConfirm() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'favorito', 'Desde: ' + url_Actual);
}

function estadisticasGAHazTuOferta() {}

function estadisticasGAHazTuOfertaConfirm() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'haz-tu-oferta', 'Desde: ' + url_Actual);
}

function estadisticasGAAlertaPrecio() {}

function estadisticasGAAlertaPrecioConfirm() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'alerta-precio', 'Desde: ' + url_Actual);
}

function estadisticasGoogleAnalitics(idOperacion, tipoInmueble, tipoOperacion, provincia, pagina) {
    var inmuebleoperacion = tipoInmueble + "-" + tipoOperacion;
    try {
        trackingAnalyticsBorrar();
        _gaq.push(['_trackPageview']);
        _gaq.push(['_addTrans', idOperacion, "", "1", "", "", "", "", ""]);
        _gaq.push(['_addItem', idOperacion, "", inmuebleoperacion, provincia, "1", "1"]);
        _gaq.push(['_trackTrans']);
    } catch (err) {}
}

function estadisticasGoogleAnaliticsBannerHome(numeroReferencia) {
    trackPageviewGenerico('/resultados/home-banners/', numeroReferencia);
}

function estadisticasGoogleAnaliticsEnlacesSEO(parametrosBusqueda) {
    trackPageviewGenerico('/links-seo/?pal=', parametrosBusqueda);
}

function estadisticasGoogleAnaliticsSolVisita(idOperacion, tipoInmueble, tipoOperacion, provincia) {
    estadisticasGoogleAnalitics(idOperacion, tipoInmueble, tipoOperacion, provincia, "confirmacion-solicitud-visita");
}

function estadisticasGoogleAnaliticsMasInfo(idOperacion, tipoInmueble, tipoOperacion, provincia) {
    estadisticasGoogleAnalitics(idOperacion, tipoInmueble, tipoOperacion, provincia, "confirmacion-formulario-visita");
}

function trackEventGenerico(categoria, accion) {
    try {
        trackingAnalyticsBorrar();
        _gaq.push(['_trackEvent', 'PRE-OTROS', categoria, accion]);
    } catch (err) {}
}

function estadisticaEventosGADescargarRevista() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'descargar-revista-pasalo', 'Desde: ' + url_Actual);
}

function estadisticaEventosGAImprimirResultado() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'imprimir-parilla-resultados', 'Desde: ' + url_Actual);
}

function estadisticaEventosGAEnviarAUnAmigo() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'enviar-amigo', 'Desde: ' + url_Actual);
}

function estadisticaEventosGAImprimirFicha() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'imprimir-ficha', 'Desde: ' + url_Actual);
}

function estadisticaEventosGACalcularFinanciacion() {
    var url_Actual = urlActual();
    trackEventoParticular('Microconversiones', 'calcular', 'Desde: ' + url_Actual);
}

function estadisticaEvntGAPrivadaConfirmaRegistro() {
    trackEventGenerico('area-privada', 'confirmacion-registro');
}

function estadisticaEvntGAPrivadaCrearBusqueda() {
    trackEventGenerico('area-privada', 'crear-busqueda');
}

function estadisticaEvntGAPrivadaSuscripcion() {
    trackEventGenerico('area-privada', 'suscripcion');
}

function estadisticaEvntGAPrivadaSolicitarBaja() {
    trackEventGenerico('area-privada', 'solicitud-baja');
}

function trackEventGA(categoria, accion) {
    trackEventGenerico(categoria, accion);
}

function trackTransactionGenerica(idTrans, nomProd, catProd) {
    try {
        trackingAnalyticsBorrar();
        _gaq.push(['_trackPageview']);
        _gaq.push(['_addTrans', idTrans, "", "1", "", "", "", "", ""]);
        _gaq.push(['_addItem', idTrans, "1", nomProd, catProd, "1", "1"]);
        _gaq.push(['_trackTrans']);
    } catch (err) {}
}

function transGASolicitudMasInfo(idOperacion, tipoOperacion, poblacion, tipoInmueble) {
    var nombreProducto = tipoOperacion + '-' + poblacion + '-' + tipoInmueble;
    var idTrans = idOperacion + '' + (new Date()).getTime();
    trackTransactionGenerica(idTrans, nombreProducto, 'solicitud-informacion');
}

function transGAPropuestaPrecio(idOperacion, tipoOperacion, poblacion, tipoInmueble) {
    var nombreProducto = tipoOperacion + '-' + poblacion + '-' + tipoInmueble;
    var idTrans = idOperacion + '' + (new Date()).getTime();
    trackTransactionGenerica(idTrans, nombreProducto, 'haznos-tu-oferta');
}

function trackBingTransaction() {
    if (!window.mstag) mstag = {
        loadTag: function() {},
        time: (new Date()).getTime()
    };
    var mstag_tops = document.createElement('script');
    mstag_tops.id = 'mstag_tops';
    mstag_tops.type = 'text/javascript';
    mstag_tops.async = true;
    mstag_tops.src = '//flex.atdmt.com/mstag/site/e177aa4a-76b8-4790-a5b5-656633f3d916/mstag.js';
    var bodytag = document.getElementsByTagName('body')[0];
    bodytag.appendChild(mstag_tops);
    mstag.loadTag("analytics", {
        dedup: "1",
        domainId: "2252659",
        type: "1",
        actionid: "119732"
    });
}
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

function trackCodigoSeguimientoTrovit() {
    jQuery('<div style=\"display:inline;\"><img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"http://rd.trovit.com/conversion/es/1/3b5e6848e4dfc42863c1ae082b66dae5/\"/></div>').appendTo('body');
}

function trackAdWordsConvertion() {
    var google_conversion_id = 1071795975;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "666666";
    var google_conversion_label = "GX4mCKmQPhCHnon_Aw";
    var google_conversion_value = 1.000000;
    var google_remarketing_only = false;
    jQuery('<div style=\"display:inline;\"><img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//www.googleadservices.com/pagead/conversion/' + google_conversion_id + '/?value=' + google_conversion_value + '&amp;label=' + google_conversion_label + '&amp;guid=ON&amp;script=0\" /></div>').appendTo('body');
}

function trackCodigoError404() {
    var urlActual = window.location.href.toString();
    _gaq.push(['_trackEvent', 'ERROR 404', 'URL: ' + urlActual, 'DESDE: ' + document.referrer, 0, true]);
}

function entradaContactarDispositivoMovil() {
    _gaq.push(['_trackEvent', 'Splash Móvil 2', 'Aterrizaje en Venta', 'Desde: ' + document.URL, 0, true]);
}

function seguirNavegando(url) {
    _gaq.push(['_trackEvent', 'Continuar navegando en Venta', 'Splash Móvil 2', 'Desde: ' + url]);
}

function seguirNavegandoOk(url) {
    _gaq.push(['_trackEvent', 'Continuar navegando en Venta', 'Splash Móvil 2', 'Desde: ' + url]);
}

function solicitudEnviadaAgradecimiento() {
    _gaq.push(['_trackPageview', '/splash-movil-2/gracias']);
}

function llamarTelefono(url) {
    _gaq.push(['_trackEvent', 'Llamada Telefónica en Venta', 'Splash Móvil 2', 'Desde: ' + url]);
}

function esUnMovil() {
    var esMovil = false;
    if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile/i) && navigator.userAgent.match(/Mobile/i)) {
        esMovil = true;
    }
    return esMovil;
}